package whizTrialPOM;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class FramePagePOM {

	public static WebDriver driver;
	String FrameAssertionexpected="WhizTrial Welcome";
	Date dt= new Date();
	SimpleDateFormat dateformat=new SimpleDateFormat("MM/dd/yyyy");
	Random rd= new Random();
	int UserID= rd.nextInt(99999);
	int password=rd.nextInt(dt.getMinutes()+dt.getSeconds());	
	int InputPhoneNumber=rd.nextInt(1234567890);
	String CurrentDate=dateformat.format(dt);
	String FrameSubmitText="successfull resgistor ur account !!!";
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public FramePagePOM(WebDriver driver) 
	{
		FramePagePOM.driver=driver;
	}
	
	// declare POM
	By RadioMale = By.id("ml");
	By SubmitBtn=By.xpath("//input[@value='Submit']");
	
	
	
	
	public void FrameTest1() throws InterruptedException 
	{
		String StoreFrameWindow= driver.getWindowHandle();
		int size = driver.findElements(By.tagName("iframe")).size();
		System.out.println("The total frames on the page:"+size);
		HomePagePOM objHome=new HomePagePOM(driver);
		Actions action=new Actions(driver);
		
		try 
		 {
			objHome.ClickFrameLink();
			assertEquals(FrameAssertionexpected, driver.getTitle());
			Thread.sleep(2000);
			driver.switchTo().frame(0);
			WebElement TableBody = driver.findElement(By.xpath("//table/tbody"));
			Select DrpProfession=new Select(driver.findElement(By.id("sel")));
			WebElement DOB=driver.findElement(By.xpath("//table/tbody/tr[8]/td[2]"));
			WebElement Address=driver.findElement(By.xpath("//table/tbody/tr[10]/td[2]"));
			//WebElement PhoneNumber=driver.findElement(By.xpath("//table/tbody/tr[6]/td[2]"));
			List <WebElement> TableForm= TableBody.findElements(By.tagName("input"));
			String [] TableValues= {"Faizan","Mamji",Integer.toString(UserID),Integer.toString(password),"faizan.mamji@arpatech.com","+92"+Integer.toString(InputPhoneNumber)};
			//System.out.println("The count of rows is :" +TableForm.size());
			
			for(int i=1; i<=TableForm.size(); i++)
			{
				int j=2;
				WebElement TableInputFields= driver.findElement(By.xpath("//table/tbody/tr["+String.valueOf(i)+"]/td["+String.valueOf(j)+"]"));
					
				if(i==1) 
				{
					action.moveToElement(TableInputFields);
					action.click();
					action.sendKeys(TableValues[0]);
					action.build().perform();
				}
				
				if(i==2) 
				{
					action.moveToElement(TableInputFields);
					action.click();
					action.sendKeys(TableValues[1]);
					action.build().perform();
				}
				
				if(i==3) 
				{
					action.moveToElement(TableInputFields);
					action.click();
					action.sendKeys(TableValues[2]);
					action.build().perform();
				}
				
				if(i==4) 
				{
					action.moveToElement(TableInputFields);
					action.click();
					action.sendKeys(TableValues[3]);
					action.build().perform();
				}
				
				if(i==5) 
				{
					action.moveToElement(TableInputFields);
					action.click();
					action.sendKeys(TableValues[4]);
					action.build().perform();
				}
				
				if(i==6) 
				{
					action.moveToElement(TableInputFields);
					action.click();
					//PhoneNumber.clear();
					action.sendKeys(TableValues[5]);
					action.build().perform();
				}
				
				if(i==7) 
				{
					driver.findElement(RadioMale).click();
				}
				
				if(i==8) 
				{
					action.moveToElement(DOB);
					action.click();
					action.sendKeys(CurrentDate);
					action.build().perform();
				}
				
				if(i==9) 
				{
					DrpProfession.selectByValue("employee");
				}
				
				if(i==10) 
				{
					action.moveToElement(Address);
					action.click();
					action.sendKeys("Karachi,Pakistan");
					action.build().perform();
				}
				
				if(i==11) 
				{
					driver.findElement(SubmitBtn).click();
					Thread.sleep(2000);
					String GetPageSource=driver.getPageSource();
					Thread.sleep(2000);
					if(GetPageSource.contains(FrameSubmitText)) 
					{
						System.out.println("Text Verified!!!");
					}
					Thread.sleep(2000);
					driver.switchTo().window(StoreFrameWindow);
					Thread.sleep(2000);
					objHome.ClickContextLink();
					//driver.switchTo().defaultContent();
					Thread.sleep(4000);
				}
			}
		 } 
		
		catch (Exception ex)
		{
			System.out.println("Exception on Frame1: " + ex.getMessage());
		}
	}
}
