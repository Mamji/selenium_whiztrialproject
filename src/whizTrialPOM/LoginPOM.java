package whizTrialPOM;

import static org.junit.Assert.assertEquals;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
//import org.openqa.selenium.interactions.Actions;

public class LoginPOM {
	
	public static WebDriver driver;
	Random rand= new Random();
	DateFormat dateformat=new SimpleDateFormat("MM/dd/yyyy");
	Date dt=new Date();
	int LoginUserID=rand.nextInt(99999)+900;
	int PhoneNumber=rand.nextInt(1234567890);
	int Password_Generate=(int) dt.getTime();
	String Current_date= dateformat.format(dt);
	String LoginPage_Assertion="WhizTrial Welcome";
	String SignupAssertion="successfull resgistor";
	String Fname="Faizan";
	String Lname="Farooq";
	String EmailAddress="faizan.mamji@arpatech.com";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}
	
	
	
	public LoginPOM(WebDriver driver)
	{
		LoginPOM.driver=driver;
	}
	
	//POM Login details
	
	By loginuname=By.id("Lid");
	By loginpassword=By.id("Lpwd");
	By submitbtn=By.id("Lsub");
	By SignupLink = By.id("N_acnt");
	By SubmitButton = By.xpath("//input[@value='Submit']");
	By LinkClick = By.linkText("Now you Log In Your Account");
	
	public void OpenSignUp() throws InterruptedException 
	{
		driver.findElement(SignupLink).click();
		WebElement TableBody= driver.findElement(By.xpath("//html/body/section/div/form/table")); //("//html/body/section/div/form/table")
		List <WebElement> rows = TableBody.findElements(By.tagName("input"));
		System.out.println("Number of rows is :" +rows.size());
		Actions action = new Actions(driver);

		//Conditions Check
		
		for(int i = 1; i<=rows.size(); i++) 
		{
			
				int j=2;
				String[] SignUpFormArray= {Fname,Lname,Integer.toString(LoginUserID),Integer.toString(Password_Generate),EmailAddress,Integer.toString(PhoneNumber)};
				WebElement TableSignUpFields=driver.findElement(By.xpath("//html/body/section/div/form/table/tbody/tr["+ String.valueOf(i)+"]/td["+String.valueOf(j)+"]"));
				WebElement RadioMale = driver.findElement(By.id("ml"));
			//	WebElement RadioFemale = driver.findElement(By.id("fml"));
				WebElement Address=driver.findElement(By.xpath("//html/body/section/div/form/table/tbody/tr[10]/td[2]"));
				WebElement DateSelection=driver.findElement(By.xpath("//html/body/section/div/form/table/tbody/tr[8]/td[2]"));
				Select drpCountry = new Select(driver.findElement(By.xpath("//select[@id='sel']")));
				//WebElement Address
				
				
				Thread.sleep(3000);

			if(i==1)
				
				{
					action.moveToElement(TableSignUpFields);
					action.click();
					action.sendKeys(SignUpFormArray[0]);
					action.build().perform();
					System.out.println("First Name:");
				}
			
			if(i==2)
				
				{
					action.moveToElement(TableSignUpFields);
					action.click();
					action.sendKeys(SignUpFormArray[1]);
					action.build().perform();
					System.out.println("Last Name:");
				}
			
			if(i==3)
				
				{
					action.moveToElement(TableSignUpFields);
					action.click();
					action.sendKeys(SignUpFormArray[2]);
					action.build().perform();
					System.out.println("UserID:");
				}
			
			if(i==4)
				
				{
					action.moveToElement(TableSignUpFields);
					action.click();
					action.sendKeys(SignUpFormArray[3]);
					action.build().perform();
					System.out.println("Password:");		
				}
			
			if(i==5)
				
				{
					action.moveToElement(TableSignUpFields);
					action.click();
					action.sendKeys(SignUpFormArray[4]);
					action.build().perform();
					System.out.println("Email:");			
				}
			
			if(i==6)
				
				{
					
					action.moveToElement(TableSignUpFields);
					action.click();
					action.sendKeys(SignUpFormArray[5]);
					action.build().perform();
					System.out.println("Phone:");
				}
			
			if(i==7)
			{
				action.moveToElement(RadioMale);
				action.click();
				action.build().perform();
				System.out.println("Radio Button Selection:");
			}
			
			if(i==8)
			{
				action.moveToElement(DateSelection);
				action.click();
				action.sendKeys(Current_date);
				action.build().perform();
				System.out.println("Date entered:");
			}
			
			if(i==9)
			{
				drpCountry.selectByValue("business");
				System.out.println("Dropdown Selection:");
			}
			
			if(i==10)
			{
				action.moveToElement(Address);
				action.click();
				action.sendKeys("Karachi,Pakistan");
				action.build().perform();
				System.out.println("Address entered Successfully:");
				Thread.sleep(2000);
				//driver.findElement(SubmitButton).click();
			}
			
			if(i==11)
			{
				driver.findElement(SubmitButton).click();
				Thread.sleep(2000);
				assertEquals(driver.getTitle(),SignupAssertion);
			}

		}
	}

	
	public void Loginpage(String Login_Username, String Login_Password) throws InterruptedException 
	{
		driver.findElement(LinkClick).click();
		Thread.sleep(2000);
		//String URl= driver.getCurrentUrl();
		driver.findElement(loginuname).sendKeys(Login_Username);
		driver.findElement(loginpassword).sendKeys(Login_Password);
		driver.findElement(submitbtn).click();
		Thread.sleep(3000);
		assertEquals(driver.getTitle(),LoginPage_Assertion);
	}

}
