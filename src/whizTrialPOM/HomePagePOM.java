package whizTrialPOM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePagePOM {
	
	public WebDriver driver;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public HomePagePOM(WebDriver driver)
	{
		this.driver=driver;
	}
		
	// HomePage POM details
	
	By clicklinktest= By.linkText("link Test");
	By clickcontexttest= By.linkText("Context Test");
	By clickdragdrop= By.linkText("Drag and Drop Test");
	By clickframetest= By.linkText("Frame Test");
	By clickwindowtest= By.linkText("Window Test");
	By clickjavascriiptalert= By.linkText("Java Script alert Test");
	
	public void ClickJavaScriptLink() 
	{
		driver.findElement(clickjavascriiptalert).click();
	}
	
	public void ClickWindowLink() 
	{
		driver.findElement(clickwindowtest).click();
	}
	
	public void ClickFrameLink() 
	{
		driver.findElement(clickframetest).click();
	}
	
	public void ClickDragDropLink() 
	{
		driver.findElement(clickdragdrop).click();
	}
	
	public void ClickContextLink() 
	{
		driver.findElement(clickcontexttest).click();
	}
	
	public void ClickLink() 
	{
		driver.findElement(clicklinktest).click();
	}


}
