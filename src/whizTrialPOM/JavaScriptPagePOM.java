package whizTrialPOM;

import static org.junit.Assert.assertEquals;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class JavaScriptPagePOM {
	
	public static WebDriver driver;
	
	String JavaScriptPage_Assertion="WhizTrial Welcome";
	String BookMyInterviewPopup="Hi! welcome to bookmyinterview";
	String JoinBookMyInterviewPopup="Are you sure you join bookmyinterview.in?";
	String YourNamePopup="please enter your name";
	String TextPopup="Faizan Mamji";

	//class object declaration
 

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
		
	public JavaScriptPagePOM(WebDriver driver) 
	{
		JavaScriptPagePOM.driver=driver;
	}
	
	//Define POM of java modal page.
	
	By BtnBookMyInterview= By.xpath("//button[@onclick='alertFunction()']");
	By BtnJoinbookmyinterview= By.xpath("//button[@onclick='confirmFunction()']");
	By BtnYourName= By.xpath("//button[@onclick='promptFunction()']");
	
	public void clickbtnBookMyInterview() throws InterruptedException 
	{
		HomePagePOM ObjHome= new HomePagePOM(driver);
		try 
		{
			ObjHome.ClickJavaScriptLink();
			Thread.sleep(5000);
			assertEquals(driver.getTitle(),JavaScriptPage_Assertion);
			Thread.sleep(2000);
			driver.findElement(BtnBookMyInterview).click();
			Thread.sleep(2000);
			Alert objalert = driver.switchTo().alert();
			assertEquals(objalert.getText(),BookMyInterviewPopup);
			Thread.sleep(3000);
			objalert.accept();
			Thread.sleep(1000);
	}
		catch (Exception ex)
		{
			System.out.println("Exception!!!" +ex.getMessage());
		}
	}
	
	public void clickbtnJoinBookMyInterview() throws InterruptedException 
	{
		try 
	    {
			driver.findElement(BtnJoinbookmyinterview).click();
			Thread.sleep(2000);
			Alert objalert = driver.switchTo().alert();
			assertEquals(objalert.getText(),JoinBookMyInterviewPopup);
			Thread.sleep(2000);
			objalert.accept();
			assertEquals(objalert.getText(),"Good!");
			Thread.sleep(1000);
			objalert.accept();
			//System.out.println("Both functions pass successfully!!!!");
		}
		catch (Exception ex)
		{
			System.out.println("Exception!!!" +ex.getMessage());
		}
	}
	
	public void clickbtnYourName() throws InterruptedException 
	{
		try 
	    {
			driver.findElement(BtnYourName).click();
			Thread.sleep(2000);
			Alert objalert = driver.switchTo().alert();
			assertEquals(objalert.getText(),YourNamePopup);
			Thread.sleep(2000);
			objalert.sendKeys(TextPopup); 
			Thread.sleep(2000);
			objalert.accept();
			assertEquals(objalert.getText(),TextPopup);
			Thread.sleep(1000);
			objalert.accept();
			System.out.println("Java Script Page Pass Successfully!!!!");
			
		}
		catch (Exception ex)
		{
			System.out.println("Exception!!!" +ex.getMessage());
		}
	}
	
	

}
