package whizTrialJunitClasses;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import webDriverGlobal.DriverClass;
import whizTrialPOM.FramePagePOM;
import whizTrialPOM.JavaScriptPagePOM;
import whizTrialPOM.LoginPOM;

class MainJavaFile extends DriverClass {
	
	//Variable Declaration
	
	public static WebDriver maindriver;
	String Loginusername="selenium";
	String Loginpassword="123456";
	
	// Class Objects Calling

	JavaScriptPagePOM ObjJava=new JavaScriptPagePOM(maindriver);
	LoginPOM ObjLPom=new LoginPOM(maindriver);
	FramePagePOM ObjFrame= new FramePagePOM(maindriver);
	
	
	

	@BeforeAll
	public static void setUp() throws Exception {
		maindriver=ChromeDriver();
		
	}

	
	@Test
	public void Test1Signup() throws InterruptedException{

		try 
		  {	
			ObjLPom.OpenSignUp();
			Thread.sleep(3000);
			//System.out.println("1st pass");
		  }
		
			catch (Exception e) {
				System.out.println("Exception Login Page!!!!" +e.getMessage());
			}
		
	}

	
	@Test
	public void Test2LoginInfo() throws InterruptedException{

		try 
		  {	
			ObjLPom.Loginpage(Loginusername,Loginpassword);
			Thread.sleep(5000);
			//System.out.println("1st pass");
		  }
		
			catch (Exception e) {
				System.out.println("Exception Login Page!!!!" +e.getMessage());
			}
		
	}
	

	@Test
	public void Test3JavaScriptPage() throws InterruptedException{
		
	try 
	  {
		ObjJava.clickbtnBookMyInterview();
		Thread.sleep(5000);
		ObjJava.clickbtnJoinBookMyInterview();
		Thread.sleep(3000);
		ObjJava.clickbtnYourName();
		Thread.sleep(2000);
      }
	
		catch (Exception e) {
				e.getMessage();
				System.out.println("Exception JavaScriptPage!!!!" +e.getMessage());
		}	
	}
	
	
	@Test
	public void Test4FramePage() throws InterruptedException{
		
	try 
	  {
		ObjFrame.FrameTest1();
		Thread.sleep(2000);
      }
	
		catch (Exception e) {
				e.getMessage();
				System.out.println("Exception FramePage!!!!" +e.getMessage());
		}	
	}
	
	
	@AfterAll
	public static void tearDown() throws Exception {
		maindriver.close();
	}
	


}
