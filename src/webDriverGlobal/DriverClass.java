package webDriverGlobal;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class DriverClass {

	public static WebDriver driver;

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}
	
	public static WebDriver ChromeDriver() throws IOException
	{
		Properties prop=new Properties();
		FileInputStream fs=new FileInputStream("E:\\Selenium_Webdriver1\\LoginWhizTrial\\Configuration\\Configuration.properties");
		prop.load(fs);
		String checkbrowser=prop.getProperty("Browser");
		String Url=prop.getProperty("PageUrl");
		//String GetUserName=prop.getProperty("username");
		//String GetPassword=prop.getProperty("password");
		String implicitwait=prop.getProperty("implicitlyWait");

		try	
		  {
			if(checkbrowser.contains("Chrome")) 
			{
			  	System.setProperty("webdriver.chrome.driver", "E:\\Java_Webdriver_Files\\chromedriver_win32\\chromedriver.exe");
				driver=new ChromeDriver();
				driver.manage().window().maximize();
				driver.navigate().to(Url);
				driver.manage().timeouts().implicitlyWait(Long.parseLong(implicitwait),TimeUnit.SECONDS);
				driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
			}
			
			else 
				{
					System.out.println("Failed to open Chrome browser");
				}
		 }
		
	  catch (Exception ex)
		  {
				System.out.println("Exception!!!" +ex.getMessage());
		  }
		return driver;
	}
}
